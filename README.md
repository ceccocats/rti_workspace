# RTI Workspace Meta Repo
Meta repo with the latest RMW for connext DDS and some helper utilities for working with RTI connext and ROS 2

## Installation

To use RTI Connext 6.1.0 with your computer: 

1. Pull this repo
1. Install RTI Connext 6.1.0 by downloading the installer [here](https://s3.amazonaws.com/RTI/Bundles/6.1.0/Evaluation/rti_connext_dds-6.1.0-lm-x64Linux4gcc7.3.0.run)
1. Run the installer as root
    - i.e. `chmod +x rti_connext_dds-6.1.0-lm-x64Linux4gcc7.3.0.run && sudo ./rti_connext_dds-6.1.0-lm-x64Linux4gcc7.3.0.run`
1. Run `install-rti.sh`

## Post-installation

To actually use the RTI DDS middleware, you need to add the following to your bashrc:

```
# RTI RMW
export CONNEXTDDS_DIR=${NDDSHOME}
source ~/rti_workspace/install/setup.bash
export RMW_IMPLEMENTATION=rmw_connextdds
```
