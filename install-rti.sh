# this script assumes you have connext dds 6.1.0 installed

source /opt/rti_connext_dds-6.1.0/resource/scripts/rtisetenv_x64Linux4gcc7.3.0.bash
CONNEXTDDS_IGNORE_BUILD=y source /opt/ros/galactic/setup.bash

# make sure everything is pulled
#git submodule update --init --recursive

#cd ~/rti_workspace && 
colcon build --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=Release

#sudo cp rti_license.dat /opt/rti_connext_dds-6.1.0/.
